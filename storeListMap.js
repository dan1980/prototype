export default {
    gb: { en: { path: '/gb/en', storeId: 10151, langId: 44, catalogId: 11551 } },
    us: { en: { path: '/us/en', storeId: 10152, langId: -1, catalogId: 12051 } },
    eu: { en: { path: '/eu/en', storeId: 10153, langId: 45, catalogId: 12551 } },
}