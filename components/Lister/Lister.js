import React, { useState, useEffect } from "react";
import AppLink from "../Custom/AppLink";

const Lister = props => {
  useEffect(() => {}, []);

  return (
    <div className="hot-lister">
      <ul className="lister">
        {props.products &&
          props.products.map(item => (
            <li key={item.title}>
              <AppLink
                href="/p/[seoname]"
                as={`/p/${item.title.toLowerCase().replace(" ", "-")}`}
              >
                <a>
                  <img src={"//hotter.com" + item.imgurl} alt={item.title} />
                </a>
              </AppLink>
              <AppLink
                href="/p/[seoname]"
                as={`/p/${item.title.toLowerCase().replace(" ", "-")}`}
              >
                <a className="title theme">
                  <h3>{item.title}</h3>
                  <p>{item.strapline}</p>
                  <p className="price">&pound;{item.sortprice}</p>
                </a>
              </AppLink>
              <div className="swatches">
                {item.swatches.map((swatch, i) => {
                  return (
                    <a key={i} title={swatch.colour}>
                      <img
                        src={"//hotter.com" + swatch.image}
                        alt={swatch.colour}
                      />
                    </a>
                  );
                })}
              </div>
            </li>
          ))}
      </ul>
    </div>
  );
};

export default Lister;
