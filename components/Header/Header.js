import React, { useEffect } from 'react';
import AppLink from '../Custom/AppLink';
import PropTypes from 'prop-types';
import StoreText from '../Custom/StoreText/StoreText';
import "./Header.scss";

const Header = props => {

    //console.log("props.navitems: ",props.navitems);
    const { navlinks } = props;

    useEffect(() => {

        //console.log("locale: ",locale);

    }, []);

    return (
        <header>
            <div className="hot-header">
                <div className="grid-container">
                    <ul className="hot-header__list">
                        {navlinks && navlinks.map((item, i) => {
                            //this will be controlled in the cms
                            return (
                                <li key={i}>
                                    <AppLink href={item.href} as={item.as ? item.as : null} reload={item.reload ? item.reload : null}>
                                        <a className="title theme">
                                            {item.title}
                                        </a>
                                    </AppLink>
                                </li>
                            );
                        })}
                        <li>
                            <a href="/gb/en" >
                                <StoreText value={props.storeText.GBSTORE} />
                            </a>
                        </li>
                        <li>
                            <a href="/us/en">
                                <StoreText value={props.storeText.USSTORE} />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    );
}
Header.getInitialProps = async () => {
    console.log("[getInitialProps]");
}

Header.propTypes = {
    navlinks: PropTypes.array,
    storeText: PropTypes.object
};

export default Header;