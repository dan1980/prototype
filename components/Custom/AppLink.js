import React from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const AppLink = props => {

    const router = useRouter();
    const { lang, locale } = router.query;
    let prefix = '';
    if(locale && lang){
        prefix = '/' + locale + '/' + lang;
    }    
    const hrefPrefix = '/[locale]/[lang]';
    const isExt = (props.href.indexOf("http") != -1);

    const renderLink = () => {
       if(props.reload){
            return (
                <Link href={isExt ? props.href : prefix + props.href}>
                    {props.children}
                </Link>  
            );
        } else if(props.as){
            return (
                <Link as={prefix + props.as} href={hrefPrefix + props.href}>
                    {props.children}
                </Link>
            );
        } else {
            return (
                <Link as={prefix + props.href} href={hrefPrefix + props.href}>
                    {props.children}
                </Link>
            );
        }
    }
  
    return (
        <>
            {renderLink()}            
        </>
    );
}
  
export default AppLink;