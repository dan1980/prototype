import React from 'react';
import PropTypes from 'prop-types';

const StoreText = props => {

    const { params } = props;
    let { value } = props;

    if (params && params.length) {
        params.forEach((param, i) => {
            value = value.replace("{" + i + "}", param);
        });
    }

    return (
        <>{value}</>
    );
}

StoreText.propTypes = {
    value: PropTypes.string,
    params: PropTypes.array
};

export default StoreText;