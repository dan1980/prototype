import { shallow } from 'enzyme';
import React from 'react';
import StoreText from './StoreText';

describe('StoreText Test Components', () => {
  const wrapper = shallow(<StoreText value="Test Value" />)
  it('StoreText Component Renders Text', () => {   
    expect(wrapper.text()).toEqual("Test Value");
  })
});

describe('StoreText Test Components', () => {
  const wrapper = shallow(<StoreText value="Param 1 value: {0}, Param 2 value: {1}" params={['Param 1','Param 2']} />)
  it('StoreText Component Renders Text with Params', () => {   
    expect(wrapper.text()).toEqual("Param 1 value: Param 1, Param 2 value: Param 2");
  })
});

describe('StoreText Test Components', () => {
  const wrapper = shallow(<StoreText value="Param 1 value: {0}, Param 2 value: {1}" params={['Param 1']} />)
  it('StoreText Component Renders Text with Params even if one param is missing', () => {   
    expect(wrapper.text()).toEqual("Param 1 value: Param 1, Param 2 value: {1}");
  })
});

describe('StoreText Test Components', () => {
  const wrapper = shallow(<StoreText value="Param 1 value: {0}, Param 2 value: {1}" params={['Param 1','Param 2','Param 3']} />)
  it('StoreText Component Renders Text with Params even if more params are provided than needed', () => {   
    expect(wrapper.text()).toEqual("Param 1 value: Param 1, Param 2 value: Param 2");
  })
});