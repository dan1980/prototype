#Checkbox Component

##import name
@wtc/CheckBox

##Description
Generic component to output a checkbox.

##Available props
name - string,
id - string,
className - string,
onChange - function,
disabled - bool,
dataQa - string, hook for testing,
value - string
defaultChecked - bool,
label: string,
labelBefore: bool, for positioning of label content,

##Example
<CheckBox
name="myCheckbox"
dataQa="myCheckboxQa"
value="myVal"
label="My checkbox label"
onChange="myFunction()"

> </CheckBox>

###Author
Danny Jordan (djordan@wundermancommerce.com)
