import React from 'react';
import { storiesOf } from '@storybook/react';
import { CheckBox } from './index';

storiesOf('@wtc/CheckBox', module)
  .add('show a Checkbox', () => (
    <CheckBox dataQa="qaTest-1" className="new-class" name="test-1" value="test-1" />
  ))
  .add('show a Checkbox with a change function', () => (
    <CheckBox onChange={() => alert('test3 changed')} name="test-3" value="test-3" />
  ))
  .add('show a Checkbox which is disabled', () => (
    <CheckBox disabled onChange={() => alert('changed')} name="test-4" value="test-4" />
  ))
  .add('show a Checkbox with a label after', () => (
    <CheckBox name="test-5" value="test-5" label="Test 5" />
  ))
  .add('show a Checkbox with a label before', () => (
    <CheckBox name="test-6" value="test-6" label="Test 6" labelBefore="true" />
  ));
