import React from 'react';
import { shallow } from 'enzyme';
import { CheckBox } from '.';

describe('checkbox component', () => {
  describe('rendering ', () => {
    describe('no props ', () => {
      let wrapper;

      beforeAll(() => {
        wrapper = shallow(<CheckBox />);
      });

      it('should render component CheckBox', () => {
        expect(wrapper.find('input[type="checkbox"]')).toHaveLength(1);
      });
    });

    describe('no value', () => {
      let wrapper;
      const noValue = '';
      const emptyValClass = 'emptyValue';
      beforeAll(() => {
        wrapper = shallow(<CheckBox className={emptyValClass} value={noValue} />);
      });

      it('should render a checkbox with no value', () => {
        expect(wrapper.find('input[type="checkbox"]').props().value).toBe(noValue);
      });
    });

    describe('render a checkbox with a value of withVal and class of notEmptyValClass', () => {
      let wrapper;
      const withVal = 'checkbox value';
      const notEmptyValClass = 'NotEmpty';
      beforeAll(() => {
        wrapper = shallow(<CheckBox className={notEmptyValClass} value={withVal} />);
      });

      it('should render a checkbox with a value', () => {
        expect(wrapper.find('input[type="checkbox"]').props().value).toBe(withVal);
      });
      it('should set notEmptyValClass class', () => {
        expect(wrapper.find('input[type="checkbox"]').hasClass('NotEmpty')).toBe(true);
      });
    });

    describe('with a label', () => {
      let wrapper;
      const chkLabel = 'my checkbox label';
      beforeAll(() => {
        wrapper = shallow(<CheckBox label={chkLabel} />);
      });

      it('should render a checkbox with a label', () => {
        expect(wrapper.find('label')).toHaveLength(1);
      });
    });

    describe('with label and disabled state', () => {
      let wrapper;
      const chkLabel = 'my checkbox label';
      beforeAll(() => {
        wrapper = shallow(<CheckBox label={chkLabel} disabled />);
      });

      it('should render a checkbox with a label and disabled state', () => {
        expect(wrapper.find('label')).toHaveLength(1);
        expect(wrapper.find('input').prop('disabled')).toBeTruthy();
      });
    });

    describe('with label before checkbox', () => {
      let wrapper;
      const chkLabel = 'my checkbox label';
      beforeAll(() => {
        wrapper = shallow(<CheckBox label={chkLabel} labelBefore />);
      });

      it('should render a checkbox with a label and disabled state', () => {
        expect(
          wrapper
            .find('label')
            .childAt(0)
            .text()
        ).toBe('my checkbox label');
      });
    });

    it('should fire onchange function', () => {
      let wrapper;
      beforeAll(() => {
        wrapper = shallow(<CheckBox />);
      });
      const eventHandler = jest.fn();
      const event = {
        preventDefault: function preventDefault() {
          return null;
        },
        target: {
          value: ''
        }
      };
      const { onChange } = CheckBox.defaultProps;

      // test onChange function
      eventHandler(onChange(event));
      expect(eventHandler).toHaveBeenCalledWith(event);
      expect(wrapper).toMatchSnapshot();
    });
  });
});
