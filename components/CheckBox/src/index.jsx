import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  name: PropTypes.string,
  id: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  dataQa: PropTypes.string,
  value: PropTypes.string,
  defaultChecked: PropTypes.bool,
  label: PropTypes.string,
  labelBefore: PropTypes.bool,
  customFace: PropTypes.any
};

const defaultProps = {
  name: "",
  id: "",
  className: "",
  onChange: r => r,
  disabled: false,
  dataQa: "CheckBox",
  value: "",
  defaultChecked: false,
  label: "",
  labelBefore: false,
  customFace: null
};

export const CheckBox = ({
  id,
  name,
  className,
  onChange,
  disabled,
  dataQa,
  value,
  defaultChecked,
  label,
  labelBefore,
  customFace
}) => {
  const disabledClass = disabled ? "-disabled" : "";
  const checkboxClass = `CheckBox ${disabledClass} ${className}`;
  const labelBeforeContent = labelBefore ? label : "";
  const labelAfterContent = !labelBefore ? label : "";
  let classNames = ["form__checkBox"];
  if (customFace) {
    classNames.push("--custom");
  }

  return (
    <div className={classNames.join(" ")}>
      <label>
        {labelBeforeContent}
        <input
          data-qa={dataQa}
          type="checkbox"
          id={id}
          name={name}
          defaultChecked={defaultChecked}
          disabled={disabled}
          className={checkboxClass}
          value={value}
          onChange={onChange}
        />
        {customFace}
        {labelAfterContent}
      </label>
    </div>
  );
};

CheckBox.propTypes = propTypes;
CheckBox.defaultProps = defaultProps;

export default CheckBox;
