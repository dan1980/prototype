import React from 'react';
import PropTypes from 'prop-types';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';


const Layout = props => {
    const { children, navlinks, storeText } = props;
    return (
        <div className="page">
            <Header navlinks={navlinks} storeText={storeText} />
            <div className="grid-container">
                {children}
            </div>
            <Footer />
        </div>
    );
};

Layout.propTypes = {
    children: PropTypes.any,
    storeText: PropTypes.object,
    navlinks: PropTypes.array
};

export default Layout;
