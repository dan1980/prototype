import React from 'react';
import PropTypes from 'prop-types';

const CheckIcon = props => {
    const classNames = ['svg', '--check'];
    if (props.className) classNames.push(props.className);

    return (
        <svg viewBox="0 0 17.5 14.5" className={classNames.join(' ')}>
            <g transform="translate(-308,-290.5)">
                <path
                    id="path3378"
                    d="m 322.30285,290.85253 3.17979,2.93954 -10.24294,11.08011 -6.88409,-6.36396 2.87894,-3.11423 3.70429,3.42441 z"
                />
            </g>
        </svg>
    )
}

CheckIcon.propTypes = {
    className: PropTypes.string
};

export default CheckIcon;