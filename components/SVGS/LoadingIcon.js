import React from 'react';
import PropTypes from 'prop-types';

const LoadingIcon = props => {

    const classNames = ['svg', '--loading'];
    if (props.className) classNames.push(props.className);

    return (
        <svg className={classNames.join(' ')} viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
            <circle fill="none" strokeWidth="8" strokeLinecap="round" cx="33" cy="33" r="29"></circle>
        </svg>
    )
}

LoadingIcon.propTypes = {
    className: PropTypes.string
};

export default LoadingIcon;