self.__precacheManifest = [
  {
    "url": "/_next/static/chunks/commons.462760e92e5367fc4ec5.js",
    "revision": "e29113754a1cf175446c"
  },
  {
    "url": "/_next/static/chunks/styles.a546e69bda8d8eff09d1.js",
    "revision": "9e90cf6fd5bdb362f4a1"
  },
  {
    "url": "/_next/static/css/commons.a62beb8b.chunk.css",
    "revision": "e29113754a1cf175446c"
  },
  {
    "url": "/_next/static/css/styles.3c246c3a.chunk.css",
    "revision": "9e90cf6fd5bdb362f4a1"
  },
  {
    "url": "/_next/static/images/local-e28e60148aa9b7672276880611417e1b.jpeg"
  },
  {
    "url": "/_next/static/runtime/main-c4f3aa936c2d6d389fb9.js",
    "revision": "b1ed096576b2640a1b55"
  },
  {
    "url": "/_next/static/runtime/webpack-9369c5c69dbf6d4912cb.js",
    "revision": "339869abd27a67efd9af"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang].js",
    "revision": "a9acc56f473036c143e3"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang]\\amp\\cat.js",
    "revision": "9102ab5679b1d2c66c5e"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang]\\cat\\[seoname].js",
    "revision": "a7f9e6c6012537745400"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang]\\checkout.js",
    "revision": "925ff973512cc5182b5d"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang]\\components.js",
    "revision": "c6421f331d5ac4af3fc8"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\[locale]\\[lang]\\p\\[seoname].js",
    "revision": "3069af8eb3f3f1bbd5b4"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\_app.js",
    "revision": "edd89d18564a8e954443"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\_error.js",
    "revision": "e1954268daaebb5174fc"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\index.js",
    "revision": "6005b4c2a40ee03dffbe"
  },
  {
    "url": "/_next/static\\xsnAsuHxMl3CkZmXT3FQ2\\pages\\legacy_error.js",
    "revision": "0b7f6c240cbf6bc59f7d"
  }
];

/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

importScripts(
  
);

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(/\.(?:nocache)$/, new workbox.strategies.NetworkOnly(), 'GET');
workbox.routing.registerRoute(/_next\/static$/, new workbox.strategies.StaleWhileRevalidate({ "cacheName":"static-resources", plugins: [new workbox.cacheableResponse.Plugin({ statuses: [ 0, 200 ] })] }), 'GET');
workbox.routing.registerRoute(/ExtendedSitesCatalogAssetStore\/images$/, new workbox.strategies.NetworkFirst({ "cacheName":"cdn-images", plugins: [new workbox.cacheableResponse.Plugin({ statuses: [ 0, 200 ] })] }), 'GET');
workbox.routing.registerRoute(/api$/, new workbox.strategies.NetworkFirst({ "cacheName":"cdn-images", plugins: [new workbox.cacheableResponse.Plugin({ statuses: [ 0, 200 ] })] }), 'GET');
