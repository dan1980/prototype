const { createServer } = require("https");
const { parse } = require("url");
const next = require("next");
const path = require("path");
const routes = require("./lib/routes");
const port = 4443;
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
//const handle = app.getRequestHandler();
const handle = routes.getRequestHandler(app);

const pem = require("https-pem");

//remove this, just for dev
process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
require("events").EventEmitter.defaultMaxListeners = 0; //will remove when server is set up correctly

app.prepare().then(() => {
  createServer(pem, (req, res) => {
    const parsedUrl = parse(req.url, true);
    const { pathname } = parsedUrl;

    // handle GET request to /service-worker.js
    if (pathname === "/service-worker.js") {
      const filePath = path.join(__dirname, ".next", pathname);
      app.serveStatic(req, res, filePath);
    } else {
      handle(req, res, parsedUrl);
    }
  }).listen(port, () => {
    console.log(`> Ready on https://localhost:${port}`);
  });
});
