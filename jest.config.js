module.exports = {
  setupFiles: ['<rootDir>/jest.setup.js'],

  testPathIgnorePatterns: ['<rootDir>/.next/', '<rootDir>/node_modules/'], // ignores test files in .next(js) & node modules

  transform: {
    "^.+\\.js$": "babel-jest", // anything .js is babel'd for jest to consume
    "^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$": "jest-transform-stub" // anything style related is ignored and mapped to jest-transform-stub module
  },
  moduleNameMapper: {
    '\\.css$': '<rootDir>/EmptyModule.js'
  }
}