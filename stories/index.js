import React from 'react';
import { storiesOf } from '@storybook/react';
import Header from '../components/Header/Header';
import Footer from '../components/Footer/Footer';
import Lister from '../components/Lister/Lister';

const headerJson = {
    "navlinks": [
        { "title": "Home", "href": "/" },
        { "title": "Womens Shoes", "as": "/cat/womens-shoes", "href": "/cat/[seoname]" },
        { "title": "Mens Shoes", "as": "/cat/mens-shoes", "href": "/cat/[seoname]" },
        { "title": "AMP Cat Page", "href": "/amp/cat" },
        { "title": "404 Example", "href": "/asdasdasddd" }
    ]
}

const listerJson = { "items": [{ "title": "Whisper Boots", "strapline": "Two zips open fully for easy access", "offerprice": "\u00a385", "sortprice": "85", "wasprice": "", "saveprice": "", "canonicalUrl": "\/gb\/en\/whisper-boots-whisps-product", "landingURL": "\/gb\/en\/whisper-boots-whisps-product?fit=Standard+Fit&colour=Maroon", "plpbadgeUrl": "\/wcsstore\/HotterStorefrontAssetStore\/content\/images\/badges\/plp\/plp-newcolours.png?v=2", "imgurl": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/large\/WHISPS.jpg", "swatches": [{ "colour": "Black", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/BLACK.gif" }, { "colour": "Navy", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/NAVY.gif" }, { "colour": "Dark Tan", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/DKTAN.gif" }, { "colour": "Maroon", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/MAROON.gif" }, { "colour": "Forest Green", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/FORGRN.gif" }, { "colour": "Urban Grey", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/URBNGRY.gif" }] }, { "title": "Rapid Shoes", "strapline": "Adjustable lace and zip closure", "offerprice": "\u00a385", "sortprice": "85", "wasprice": "", "saveprice": "", "canonicalUrl": "\/gb\/en\/rapid-shoes", "landingURL": "\/gb\/en\/rapid-shoes?fit=Standard+Fit&colour=Urban Grey Multi", "plpbadgeUrl": "\/wcsstore\/HotterStorefrontAssetStore\/content\/images\/badges\/plp\/plp-new.png?v=2", "imgurl": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/large\/RAPIDS.jpg", "swatches": [{ "colour": "Black Multi", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/BLKMLTI.gif" }, { "colour": "Plum Multi", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/Plum Multi.gif" }, { "colour": "Urban Grey Multi", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/URGRYML.gif" }] }, { "title": "Ellery Boots", "strapline": "Same fit with a new slimline sole", "offerprice": "\u00a385", "sortprice": "85", "wasprice": "", "saveprice": "", "canonicalUrl": "\/gb\/en\/ellery-boots-elleys-product--1", "landingURL": "\/gb\/en\/ellery-boots-elleys-product--1?fit=Standard+Fit&colour=Dark Tan", "plpbadgeUrl": "\/wcsstore\/HotterStorefrontAssetStore\/content\/images\/badges\/plp\/plp-stylerefresh.png?v=2", "imgurl": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/large\/ELLEYS.jpg", "swatches": [{ "colour": "Black", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/BLACK.gif" }, { "colour": "Maroon", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/MAROON.gif" }, { "colour": "Dark Tan", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/DKTAN.gif" }, { "colour": "Dark Pewter", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/DKPEWTE.gif" }, { "colour": "Navy", "image": "\/wcsstore7.00.00.1209\/ExtendedSitesCatalogAssetStore\/images\/products\/swatches\/NAVY.gif" }] }] };

storiesOf('Components|Global', module)
    .add('Header', () => (
        <Header navlinks={headerJson.navlinks} />
    )).add('Footer', () => (
        <Footer />
    )).add('Lister', () => (
        <Lister products={listerJson.items} />
    ));