import axios from 'axios';

const defaultParams = {
    headers: { 'Content-Type': 'application/json' },
    mode: 'cors',
    cache: 'default'
};

export const callApi = async () => {
    //main api call here

    //PORT ACROSS ESTABISHED EXECUTE FETCH CALL HERE
    //return executeFetch(path, params, authenticated);

}

export const apiUrls = {
    GET_SEO: '/static/mock/seo.json',
    GET_NAV_LINKS: '/static/mock/navlinks.json',
    GET_PDP_DATA: '/static/mock/pdp.json',
    GET_PLP_DATA: '/static/mock/plp.json',
    GET_SEO_DATA: '/static/mock/seo.json',
    GET_USER_DATA: '/static/mock/user.json',
}
export default callApi;