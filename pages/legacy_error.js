import React, { useEffect } from 'react';
import Head from 'next/head';
import Header from '../components/Header/Header';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { baseActions, getHeaderData } from '../lib/store/actions/index';
import "../assets/scss/core.scss";


const Error = props => {

    //console.log("Env var: ",process.env.REACT_APP_EXAMPLE);
    useEffect(() => {



    }, []);

    return (
        <>
            <Head>
                <title>Hotter.com - Oops something went wrong.</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <div className="container">
                <Header navlinks={props.navlinks} storeText={props.storeText} />
                <h1>Oops something went wrong</h1>

            </div>
        </>
    );
}

Error.propTypes = {
    storeText: PropTypes.object,
    userJson: PropTypes.object,
    navlinks: PropTypes.any
};

Error.getInitialProps = async function ({ store, query: { lang, locale } }) {
    //global base redux dispatches
    await baseActions(store, locale, lang);

    await Promise.all([
        store.dispatch(getHeaderData())
    ]);
};

const mapStateToProps = state => {
    return {
        storeText: state.storeText.data,
        userData: state.userData
    }
}

export default connect(mapStateToProps)(Error);