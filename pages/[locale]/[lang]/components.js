import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Link from 'next/link';
import { connect } from "react-redux";
import { baseActions, getHeaderData } from '../../../lib/store/actions/index';
import Layout from '../../../components/Layout/Layout';
import "../../../assets/scss/core.scss";
import "../../../assets/scss/_component-library.scss";
import Button from '@wtc/Button';
import TextInput from '@wtc/TextInput';
//import { validateFilled } from '@wtc/Utils';
import CheckIcon from '../../../components/SVGS/CheckIcon';
import LoadingIcon from '../../../components/SVGS/LoadingIcon';
import PadlockIcon from '../../../components/SVGS/PadlockIcon';
import HeartIcon from '../../../components/SVGS/HeartIcon';
import Logo from '../../../components/SVGS/Logo';
import SearchIcon from '../../../components/SVGS/SearchIcon';
import CheckBox from '@wtc/CheckBox';
import RadioInput from '@wtc/RadioInput';
import SelectInput from '@wtc/SelectInput';
import LocalImg from '../../../assets/img/local.jpeg';



const Components = props => {

    const { header, storeText } = props;
    const initialState = {
        form : {
            emailAddress : {
                errorCode: null,
                value: '',
                rules: ['validFilled','validEmail']
            },
            test1 : {
                errorCode: null,
                value: '',
                rules: ['validFilled']
            },
            selexample : {
                errorCode: null,
                value: '',
                rules: ['validFilled']
            },
        }
    }
    const [form, setForm] = useState(initialState.form); 

    //move validators to Utils
    const validators = {
        validEmail: (value) => {
            const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            return value.trim() === '' || emailRegex.test(value);
        },
        validFilled: value => value.length,
    }

    //move validateUnit to Utils
    const validateUnit = (rules,val) => {
        let errorCode = null;
        rules.forEach(validationName => { 
            if(!errorCode){
                const isValid = validators[validationName](val);
                errorCode = !isValid ? validationName : null; 
            }                   
        });
        return errorCode;
    }

    const updateInput = event => {
        
        const ref = event.target.name;
        const val = event.target.value;
        const field = form[ref];
        const errorCode = validateUnit(field.rules,val);
        
        setForm(prevState => ({
            ...prevState, [ref]: { ...prevState[ref], errorCode: errorCode, value: val }                       
        }));
    }
    
   

    const errorMapping = {
        validFilled: storeText.ERR_REQUIREDFIELD,
        validEmail: storeText.ERR_INVALIDEMAIL,
    };


    return (
        <>
            <Head>
                <title>Hotter Prototype - Next JS</title>
            </Head>
            <Layout navlinks={header.navlinks} storeText={storeText}>

                <h1>Components</h1>


                <ol>
                    <li><Link href="#typography"><a>Typography</a></Link></li>
                    <li><Link href="#buttons"><a>Buttons</a></Link></li>
                    <li><Link href="#forms"><a>Forms</a></Link></li>
                    <li><Link href="#icons"><a>Icons</a></Link></li>
                    <li><Link href="#imagery"><a>Imagery</a></Link></li>
                    <li><Link href="#tables"><a>Tables</a></Link></li>
                </ol>


                <div className="hot-comp" id="typography">
                    <h2 className="hot-comp__title">Typography</h2>
                    <div className="grid-x grid-margin-x grid-margin-y">
                        <div className="cell small-12 large-4">
                            <h1>Heading 1</h1>
                            <h2>Heading 2</h2>
                            <h3>Heading 3</h3>
                            <h4>Heading 4</h4>
                            <h5>Heading 5</h5>
                            <h6>Heading 6</h6>
                            <hr />
                            <p>Paragraph text <a href="#example">with a link</a>.</p>
                            <p>Global font: Din Light, Heading font: Din Medium</p>
                        </div>
                        <div className="cell small-12 large-4">
                            <ul>
                                <li>Unordered List Item 1</li>
                                <li>Unordered List Item 2
                                    <ul>
                                        <li>Nested list item</li>
                                        <li>Nested list item</li>
                                        <li>Nested list item</li>
                                    </ul>
                                </li>
                                <li>Unordered List Item 3</li>
                            </ul>
                            <hr />
                            <ul className="no-bullet">
                                <li>No Bullet Unordered List Item 1</li>
                                <li>No Bullet Unordered List Item 2</li>
                                <li>No Bullet Unordered List Item 3</li>
                            </ul>
                        </div>
                        <div className="cell small-12 large-4">
                            <ol>
                                <li>Ordered List Item 1</li>
                                <li>Ordered List Item 2</li>
                                <li>Ordered List Item 3</li>
                                <li>Ordered List Item 4</li>
                                <li>Ordered List Item 5</li>
                                <li>Ordered List Item 6</li>
                            </ol>
                            <hr />
                            <dl>
                                <dt>Definition List - 1</dt>
                                <dd>Here is the definition data.</dd>
                                <dt>Definition List - 2</dt>
                                <dd>Here is the definition data.</dd>
                                <dd>Maybe another line of data.</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div className="hot-comp" id="buttons">
                    <h2 className="hot-comp__title">Buttons</h2>
                    <div className="grid-x grid-margin-x grid-margin-y">
                        <div className="cell small-12 large-3">
                            <Button className="blocked">Primary</Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="secondary blocked">Secondary</Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="tertiary blocked">Tertiary</Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="quaternary blocked">Quaternary</Button>
                        </div>
                    </div>
                    <div className="grid-x grid-margin-x grid-margin-y">
                        <div className="cell large-3 small-12">
                            <Button className="quaternary super blocked">Super</Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="primary large blocked loading">Large Loading<LoadingIcon /></Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="secondary blocked disabled">Disabled</Button>
                        </div>
                        <div className="cell large-3 small-12">
                            <Button className="secondary small">Small</Button>
                        </div>
                    </div>
                </div>

                <div className="hot-comp" id="forms">
                    <h2 className="hot-comp__title">Forms</h2>
                    <form className="form">
                        <div className="grid-x grid-margin-x grid-margin-y">
                            <div className="cell large-4 small-12">
                                <h3>Inputs</h3>
                                <TextInput
                                    onBlur={updateInput}
                                    errorCode={form.test1.errorCode}
                                    errorMapping={errorMapping}
                                    value=""
                                    autocomplete="address-line1"       
                                    name="test1"
                                    id="test1"
                                    label="Text Input v1"
                                    labelBefore />
                                <TextInput
                                    value=""
                                    name="test2"
                                    id="test2"
                                    label="Text Input"
                                    labelBefore
                                    description="(not required)" />
                                <TextInput
                                    value=""
                                    name="test3"
                                    id="test3"
                                    label="Text Input"
                                    labelBefore
                                    placeholder="Place holder text"
                                />
                                <TextInput
                                    onBlur={updateInput}
                                    errorCode={form.emailAddress.errorCode}
                                    errorMapping={errorMapping}                                    
                                    value={form.emailAddress.value}
                                    name="emailAddress"
                                    id="test3a"
                                    label="Email Address"
                                    labelBefore
                                />
                                <TextInput
                                    name="test4"
                                    id="test4"
                                    label="Disabled Text Input"
                                    labelBefore
                                    disabled
                                />
                            </div>
                            <div className="cell large-4 small-12">
                                <h3>Check Boxes</h3>
                                <CheckBox
                                    label="Checkbox 1"
                                    dataQa="checkbox-example"
                                    id="example1"
                                    name="example1"
                                    customFace={(
                                        <span className="form__checkBox-face"><CheckIcon /></span>
                                    )}
                                />
                                <CheckBox
                                    label="Checkbox 2"
                                    dataQa="checkbox-example2"
                                    id="checkbox-example2"
                                    name="example2"
                                    defaultChecked
                                    customFace={(
                                        <span className="form__checkBox-face"><CheckIcon /></span>
                                    )}
                                />
                                <h4 className="hot-comp__subhead">Grouped</h4>
                                <div className="form__checkBox-group">
                                    <CheckBox
                                        label="Group Item 1"
                                        dataQa="checkbox-example"
                                        id="checkbox-example3"
                                        name="example3"
                                        customFace={(
                                            <span className="form__checkBox-face"><CheckIcon /></span>
                                        )}
                                    />
                                    <CheckBox
                                        label="Group Item 2"
                                        dataQa="checkbox-example4"
                                        id="checkbox-example4"
                                        name="example4"
                                        customFace={(
                                            <span className="form__checkBox-face"><CheckIcon /></span>
                                        )}
                                    />
                                    <CheckBox
                                        label="Group Item 3"
                                        dataQa="checkbox-example"
                                        id="checkbox-example5"
                                        name="example5"
                                        customFace={(
                                            <span className="form__checkBox-face"><CheckIcon /></span>
                                        )}
                                    />
                                </div>
                            </div>
                            <div className="cell large-4 small-12">
                                <h3>Radio Buttons</h3>
                                <RadioInput
                                    label="Radio 1"
                                    value="1"
                                    defaultSelection="1"
                                    name="radio-example1" />
                                <RadioInput
                                    label="Radio 2"
                                    value="2"
                                    name="radio-example1" />
                                <h4 className="hot-comp__subhead">Grouped</h4>
                                <div className="radio__group">
                                    <RadioInput
                                        label="Radio Group 1"
                                        value="1"
                                        defaultSelection="1"
                                        name="radio-example2" />
                                    <RadioInput
                                        label="Radio Group 2"
                                        value="2"
                                        name="radio-example2" />
                                </div>
                            </div>
                        </div>
                        <div className="grid-x grid-margin-x grid-margin-y">
                            <div className="cell large-4 small-12">
                                <h3>Select Boxes</h3>
                                <SelectInput
                                    label="Select Box"
                                    labelBefore
                                    onBlur={updateInput}
                                    onChange={updateInput}
                                    errorCode={form.selexample.errorCode}
                                    errorMapping={errorMapping}                                    
                                    value={form.selexample.value}
                                    name="selexample"                                    
                                    id="selexample"
                                    selectAttributes={{
                                        autocomplete: 'test',
                                    }}
                                    placeholder="Please Select"
                                    options={[
                                        { value: 'test1', label: 'Option Value 1' },
                                        { value: 'test2', label: 'Option Value 2' },
                                        { value: 'test3', label: 'Option Value 3' }
                                    ]}
                                />
                            </div>
                            <div className="cell large-4 small-12">

                            </div>
                            <div className="cell large-4 small-12">

                            </div>
                        </div>
                    </form>

                </div>

                <div className="hot-comp" id="icons">
                    <h2 className="hot-comp__title">Icons</h2>
                    <CheckIcon className="hot-comp__icon" />
                    <LoadingIcon className="hot-comp__icon" />
                    <PadlockIcon className="hot-comp__icon" />
                    <HeartIcon className="hot-comp__icon" />
                    <HeartIcon className="hot-comp__icon" fill />
                    <SearchIcon className="hot-comp__icon" />
                </div>

                <div className="hot-comp" id="imagery">
                    <h2 className="hot-comp__title">Imagery</h2>
                    <div className="grid-x grid-margin-x grid-margin-y">
                        <div className="cell large-6 small-12">
                            <p><img src={LocalImg} alt="Locally served images" /></p>
                            <p>Locally served jpeg image.</p>
                        </div>
                        <div className="cell large-6 small-12">
                            <p><Logo className="--small" /></p>
                            <p>Small Hotter Logo SVG.</p>
                            <p><Logo /></p>
                            <p>Medium Hotter Logo SVG.</p>
                            <p><Logo className="--large" /></p>
                            <p>Large Hotter Logo SVG.</p>
                        </div>
                    </div>
                </div>

                <div className="hot-comp" id="tables">
                    <h2 className="hot-comp__title">Tables</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Table Header</th>
                                <th>Table Header</th>
                                <th>Table Header</th>
                                <th>Table Header</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer content Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                            <tr>
                                <td>Content Goes Here</td>
                                <td>This is longer Content Goes Here Donec id elit non mi porta gravida at eget metus.</td>
                                <td>Content Goes Here</td>
                                <td>Content Goes Here</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                {/*<div className="hot-comp">
                    <h2>Login Form</h2>
                    <LoginForm />
                </div>*/}
            </Layout>
        </>
    );
}

Components.getInitialProps = async function ({ store, query: { lang, locale } }) {

    await baseActions(store, locale, lang);

    await Promise.all([
        store.dispatch(getHeaderData())
    ]);

    return {};
};

Components.propTypes = {
    userData: PropTypes.object,
    storeText: PropTypes.object,
    seoData: PropTypes.object,
    navlinks: PropTypes.any,
    header: PropTypes.object,
};

const mapStateToProps = state => {
    return {
        storeText: state.storeText.data,
        userData: state.userData,
        seoData: state.seoData,
        header: state.header,
    }
}

export default connect(mapStateToProps)(Components);