export const config = { amp: true }
import React from 'react';
import Link from 'next/link';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import Layout from '../../../../components/Layout/Layout';
import { baseActions, getHeaderData, getPlpData } from '../../../../lib/store/actions/index';


const Lister = props => {

    const { header, plpdata, storeText } = props;

    return (
        <>
            <Layout navlinks={header.navlinks} storeText={storeText}>
                <h1>AMP Product Lister SSR (next.js)</h1>
                <ul className="lister">
                    {plpdata.items.map(item => (
                        <li key={item.title}>
                            <Link href={`/gb/en/p/${item.title}`}>
                                <a><amp-img width="200" height="200" src={'//hotter.com' + item.imgurl} alt={item.title} /></a>
                            </Link>
                            <Link href={`/gb/en/p/${item.title}`}>
                                <a className="title theme">
                                    <h3>{item.title}</h3>
                                    <p>{item.strapline}</p>
                                    <p className="price">&pound;{item.sortprice}</p>
                                </a>
                            </Link>
                            <div className="swatches">
                                {item.swatches.map((swatch, i) => {
                                    return (
                                        <a key={i} title={swatch.colour}><amp-img width="40" height="40" src={'//hotter.com' + swatch.image} alt={swatch.colour} /></a>
                                    );
                                })}

                            </div>
                        </li>
                    ))}
                </ul>
            </Layout>
        </>
    );
}

Lister.getInitialProps = async ({ store, query: { locale, lang, refine } }) => {


    //global base redux dispatches
    await baseActions(store, locale, lang);

    await Promise.all([
        store.dispatch(getHeaderData()),
        store.dispatch(getPlpData(refine))
    ]);

    return {}
};

Lister.propTypes = {
    plpdata: PropTypes.object,
    seoname: PropTypes.string,
    pageHeading: PropTypes.string,
    canonical: PropTypes.string,
    keywords: PropTypes.string,
    pageDesc: PropTypes.string,
    pageTitle: PropTypes.string,
    header: PropTypes.object,
    storeText: PropTypes.object,
    items: PropTypes.any
};


const mapStateToProps = state => {
    return {
        storeText: state.storeText.data,
        userData: state.userData,
        seoData: state.seoData,
        header: state.header,
        plpdata: state.plpdata
    }
}

export default connect(mapStateToProps)(Lister);