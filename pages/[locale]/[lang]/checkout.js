import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { connect } from "react-redux";
import { baseActions, getHeaderData } from '../../../lib/store/actions/index';
import Layout from '../../../components/Layout/Layout';
import "../../../assets/scss/core.scss";


const Checkout = props => {

    const { header, storeText } = props;

    return (
        <>
            <Head>
                <title>Checkout Page</title>
            </Head>
            <Layout navlinks={header.navlinks} storeText={storeText}>
                <h1>Checkout</h1>
            </Layout>
        </>
    );
}

Checkout.getInitialProps = async function ({ store, query: { locale, lang } }) {

    await baseActions(store, locale, lang); //global base redux dispatches

    await Promise.all([
        store.dispatch(getHeaderData())
    ]);

    return {};
};

Checkout.propTypes = {
    storeText: PropTypes.object,
    userJson: PropTypes.object,
    header: PropTypes.object
};



const mapStateToProps = state => {
    return {
        storeText: state.storeText.data,
        userData: state.userData,
        header: state.header
    }
}

export default connect(mapStateToProps)(Checkout);