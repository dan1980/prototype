import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { connect } from "react-redux";
import { baseActions, getHeaderData, getPlpData } from '../../../../lib/store/actions/index';
import Layout from '../../../../components/Layout/Layout';
import Lister from '../../../../components/Lister/Lister';
import "../../../../assets/scss/core.scss";


const ProductLister = props => {


    const { seoData, header, plpdata, storeText } = props;


    return (
        <>
            <Head>
                <title>{seoData.pageTitle}</title>
                <link rel="amphtml" href="/amp/cat" />
                <meta name="description" content={seoData.pageDesc} />
                <meta name="keywords" content={seoData.keywords} />
                <link rel="canonical" href={seoData.canonical} />
            </Head>
            <Layout navlinks={header.navlinks} storeText={storeText}>
                <h1>{seoData.pageHeading ? seoData.pageHeading : 'None set'}</h1>
                <Lister products={plpdata.items} />
                {/*<cms contentspot=""/>*/}
            </Layout>
        </>
    );
}

ProductLister.getInitialProps = async ({ store, query: { locale, lang, refine } }) => {


    //global base redux dispatches
    await baseActions(store, locale, lang);

    await Promise.all([
        store.dispatch(getHeaderData()),
        store.dispatch(getPlpData(refine))
    ]);

    return {}
};


ProductLister.propTypes = {
    seoData: PropTypes.object,
    header: PropTypes.object,
    storeText: PropTypes.object,
    plpdata: PropTypes.object
};

const mapStateToProps = state => {
    return {
        storeText: state.storeText.data,
        userData: state.userData,
        seoData: state.seoData,
        header: state.header,
        plpdata: state.plpdata
    }
}

export default connect(mapStateToProps)(ProductLister);