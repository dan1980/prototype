import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Head from "next/head";
import { connect } from "react-redux";
import { baseActions, getHeaderData } from "../../../lib/store/actions/index";
import Layout from "../../../components/Layout/Layout";
import StoreText from "../../../components/Custom/StoreText/StoreText";
import "../../../assets/scss/core.scss";
import LocalImg from "../../../assets/img/local.jpeg";
import Logo from "../../../components/SVGS/Logo";

const Index = props => {
  const { header, storeText, userData } = props;

  return (
    <>
      <Head>
        <title>Hotter Prototype - Next JS</title>
      </Head>
      <Layout navlinks={header.navlinks} storeText={storeText}>
        <h1>Home</h1>
        <p>
          <StoreText value={storeText.MINIBASKET} />
        </p>
        <p>
          <StoreText
            value={storeText.INTRO}
            params={[userData.userName, "Hotter"]}
          />
        </p>
        <div className="grid-x grid-margin-x grid-margin-y">
          <div className="cell large-6 small-12">
            <p>
              <img src={LocalImg} alt="Locally served images" />
            </p>
            <p>Locally served jpeg image.</p>
          </div>
          <div className="cell large-6 small-12">
            <p>
              <Logo className="--small" />
            </p>
            <p>Small Hotter Logo SVG.</p>
            <p>
              <Logo />
            </p>
            <p>Medium Hotter Logo SVG.</p>
            <p>
              <Logo className="--large" />
            </p>
            <p>Large Hotter Logo SVG.</p>
          </div>
        </div>
      </Layout>
    </>
  );
};

Index.getInitialProps = async function({ store, query: { lang, locale } }) {
  //console.log("process: .env", process.env);
  //global base redux dispatches
  await baseActions(store, locale, lang);

  await Promise.all([store.dispatch(getHeaderData())]);

  return {};
};

Index.propTypes = {
  userData: PropTypes.object,
  storeText: PropTypes.object,
  seoData: PropTypes.object,
  header: PropTypes.object
};

const mapStateToProps = state => {
  return {
    storeText: state.storeText.data,
    userData: state.userData,
    seoData: state.seoData,
    header: state.header
  };
};

export default connect(mapStateToProps)(Index);
