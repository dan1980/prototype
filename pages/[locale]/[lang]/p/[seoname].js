import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Layout from '../../../../components/Layout/Layout';
import { connect } from "react-redux";
import { baseActions, getHeaderData, getPdpData } from '../../../../lib/store/actions/index';
import "../../../../assets/scss/core.scss";

const Product = props => {

  const { pdpdata: { title, desc, image, price }, header, storeText } = props;

  return (
    <>
      <Head>
        <title>{title} | Hotter.com</title>
      </Head>
      <Layout navlinks={header.navlinks} storeText={storeText}>
        <h1>{title} - £{price}</h1>
        <p>{desc}</p>
        <img src={image} alt={title} />
      </Layout>
    </>
  );
}

Product.getInitialProps = async function ({ store, query: { locale, lang } }) {

  await baseActions(store, locale, lang);

  await Promise.all([
    store.dispatch(getHeaderData()),
    store.dispatch(getPdpData())
  ]);

  return {};
};


Product.propTypes = {
  pdpdata: PropTypes.object,
  header: PropTypes.any,
  storeText: PropTypes.object,
  items: PropTypes.any,
  price: PropTypes.number,
  title: PropTypes.string,
  image: PropTypes.string,
  desc: PropTypes.string
};

const mapStateToProps = state => {
  return {
    storeText: state.storeText.data,
    userData: state.userData,
    header: state.header,
    pdpdata: state.pdpdata
  }
}

export default connect(mapStateToProps)(Product);