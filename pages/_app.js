import React from 'react';
import App from 'next/app';
import redirect from '../lib/redirect';
import isValidStore from '../lib/isValidStore';
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import { makeStore } from "../lib/store/store";

export default withRedux(makeStore, { debug: true })(class Hotter extends App {

  static async getInitialProps({ Component, ctx }) {

    //console.log("[ctx]", ctx);
    const { lang, locale } = ctx.query;
    //check valid store, if not push them to geo ip home    
    if (!isValidStore(locale, lang)) {

      //const defaultStore = await getDefaultStore(); //here we need to do the geo maxmind look up
      const defaultStore = '/gb/en';
      defaultStore && redirect(ctx, defaultStore);
    }

    let pageProps = {}
    //now the page level props
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps }
  }


  render() {
    const { Component, pageProps, store } = this.props;

    return (
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    );
  }
});
