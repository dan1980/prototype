const withSass = require("@zeit/next-sass");
const withOffline = require("next-offline");
const withImages = require("next-images");
const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true"
});
require("dotenv").config();
const webpack = require("webpack");

const nextConfig = {
  webpack: (config, { defaultLoaders }) => {
    config.plugins.push(new webpack.EnvironmentPlugin(process.env));
    config.resolve.symlinks = false;
    config.module.rules.push({
      test: /.jsx/,
      exclude: /node_modules\/(?!@wtc|@dfs-ui|@hotter).*?$/,
      use: defaultLoaders.babel
    });
    return config;
  },
  sassLoaderOptions: {
    includePaths: ["node_modules/foundation-sites/scss"]
  },
  workboxOpts: {
    runtimeCaching: [
      {
        urlPattern: /_next\/static$/,
        handler: "StaleWhileRevalidate",
        options: {
          cacheName: "static-resources",
          expiration: {
            maxEntries: 200,
            maxAgeSeconds: 60 * 60 * 7
          },
          cacheableResponse: { statuses: [0, 200] }
        }
      },
      {
        urlPattern: /ExtendedSitesCatalogAssetStore\/images$/,
        handler: "NetworkFirst",
        options: {
          cacheName: "cdn-assets",
          expiration: {
            maxEntries: 200,
            maxAgeSeconds: 60 * 60 * 7
          },
          cacheableResponse: { statuses: [0, 200] }
        }
      },
      {
        urlPattern: /resources\/store$/,
        handler: "NetworkFirst",
        options: {
          cacheName: "api-calls",
          expiration: {
            maxEntries: 30,
            maxAgeSeconds: 60 * 60
          },
          cacheableResponse: { statuses: [0, 200] }
        }
      }
    ]
  }
};

module.exports = withBundleAnalyzer(
  withSass(withImages(withOffline(nextConfig)))
);
