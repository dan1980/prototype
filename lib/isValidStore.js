import storeListMap from '../storeListMap';

//test to make sure its a valid store
export default (locale, lang) => {
    return storeListMap[locale] && storeListMap[locale][lang];
}