import { combineReducers } from "redux";
import wcStoreReducer from './wcstore';
import userDataReducer from './userdata';
import storeTextReducer from './storetext';
import seoReducer from './seodata';
import headerReducer from './header';
import plpReducer from './plpdata';
import pdpReducer from './pdpdata';

const rootReducer = combineReducers({
    wcStore: wcStoreReducer,
    userData: userDataReducer,
    storeText: storeTextReducer,
    seoData: seoReducer,
    header: headerReducer,
    plpdata: plpReducer,
    pdpdata: pdpReducer
});

export default rootReducer;