import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  pageTitle: null,
  pageHeading: null,
  pageDesc: null,
  keywords: null,
  canonical: null,
  categoryId: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETSEODATA:
            return updateObject(state, {
              pageTitle: action.pageTitle,
              pageHeading: action.pageHeading,
              pageDesc: action.pageDesc,
              keywords: action.keywords,
              canonical: action.canonical,
              categoryId: action.categoryId
            });
        default:
            return state;
    }
};

export default reducer;