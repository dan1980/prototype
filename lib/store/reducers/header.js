import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    navlinks: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETHEADERDATA:
            return updateObject(state, {
                navlinks: action.navlinks
            });
        default:
            return state;
    }
};

export default reducer;