import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  items: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETPLPDATA:
            return updateObject(state, {
              items: action.items
            });
        default:
            return state;
    }
};

export default reducer;