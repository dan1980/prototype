import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
  ID: null,
  title: null,
  price: null,
  desc: null,
  image: null 
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETPDPDATA:
            return updateObject(state, {
              ID: action.ID,
              title: action.title,
              price: action.price,
              desc: action.desc,
              image: action.image 
            });
        default:
            return state;
    }
};

export default reducer;