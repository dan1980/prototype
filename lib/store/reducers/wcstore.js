import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    storeId: null,
    catalogId: null,
    langId: null,
    domain: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETWCSTORE:
            return updateObject(state, {
                storeId: action.storeId,
                catalogId: action.catalogId,
                langId: action.langId,
                domain: action.domain
            });
        default:
            return state;
    }
};

export default reducer;