import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    data: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETSTORETEXT:
            return updateObject(state, {
                data: action.data
            });
        default:
            return state;
    }
};

export default reducer;