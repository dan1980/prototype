import actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {
    userType: null,
    userName: null,
    authenticated: null
};

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GETUSERDATA:
            return updateObject(state, {
                userType: action.userType,
                userName: action.userName,
                authenticated: action.authenticated
            });
        default:
            return state;
    }
};

export default reducer;