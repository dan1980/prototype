import actionTypes from './actionTypes';
import axios from 'axios';
import { apiUrls } from '../../../api/index';

export const getHeaderData = () => {
    return (dispatch, getState) => axios.get(getState().wcStore.domain + apiUrls.GET_NAV_LINKS)
        .then(({ data }) => data)
        .then(data => dispatch({
            type: actionTypes.GETHEADERDATA,
            navlinks: data.navlinks
          }));
}