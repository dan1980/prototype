import actionTypes from './actionTypes';
import axios from 'axios';
import { apiUrls } from '../../../api/index';

export function getPdpData() {

  return (dispatch, getState) => axios.get(getState().wcStore.domain + apiUrls.GET_PDP_DATA)
  .then(({ data }) => data)
  .then(pdpdata => dispatch({
        type: actionTypes.GETPDPDATA,
        ID: pdpdata.ID,
        title: pdpdata.title,
        price: pdpdata.price,
        desc: pdpdata.desc,
        image: pdpdata.image
      }));
}