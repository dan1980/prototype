import actionTypes from './actionTypes';
import axios from 'axios';

//need to reassess how to get this into the build

export function getStoreText(locale, lang) {
    return (dispatch, getState) => axios.all([
        axios.get(getState().wcStore.domain + '/static/storetext/properties.json'),
        axios.get(getState().wcStore.domain + '/static/storetext/' + locale + '-' + lang + '-properties.json')
    ])
        .then(axios.spread((base, storeSpecific) => dispatch({
            type: actionTypes.GETSTORETEXT,
            data: { ...base.data, ...storeSpecific.data }
        })));
}