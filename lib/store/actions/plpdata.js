import actionTypes from './actionTypes';
import axios from 'axios';
import { apiUrls } from '../../../api/index';

export function getPlpData(refine) {

  //plp results filters - move this to api file
  let dataQuery = '';

  if (refine) {
      const refinements = refine.split("|");
      dataQuery = '?' + refinements.map((filter, i) => {
          const [name, val] = filter.split(":")
          const ampersand = (i === 0) ? '' : '&'
          return ampersand + name + '=' + val;
      });
  }

  return (dispatch, getState) => axios.get(getState().wcStore.domain + apiUrls.GET_PLP_DATA + dataQuery)
  .then(({ data }) => data)
  .then(plpdata => dispatch({
        type: actionTypes.GETPLPDATA,
        items: plpdata.items
      }));
}