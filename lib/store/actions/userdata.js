import actionTypes from './actionTypes';
import axios from 'axios';
import { apiUrls } from '../../../api/index';

export function getUserData() {
    return (dispatch, getState) => axios.get(getState().wcStore.domain + apiUrls.GET_USER_DATA)
        .then(({ data }) => data)
        .then(userdata => dispatch({
            type: actionTypes.GETUSERDATA,
            userType: userdata.userType,
            userName: userdata.userName,
            authenticated: userdata.authenticated
        }));
}