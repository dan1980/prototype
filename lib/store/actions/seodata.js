import actionTypes from './actionTypes';
import axios from 'axios';
import { apiUrls } from '../../../api/index';

export function getSeoData() {
  return (dispatch, getState) => axios.get(getState().wcStore.domain + apiUrls.GET_SEO_DATA)
  .then(({ data }) => data)
  .then(seodata => dispatch({
      type: actionTypes.GETSEODATA,
      pageTitle: seodata.pageTitle,
      pageHeading: seodata.pageHeading,
      pageDesc: seodata.pageDesc,
      keywords: seodata.keywords,
      canonical: seodata.canonical,
      categoryId: seodata.categoryId
  }));
}