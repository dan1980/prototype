import { getWCStore } from "./wcstore";
import { getUserData } from "./userdata";
import { getStoreText } from "./storetext";
import { getSeoData } from "./seodata";

export const baseActions = async (store, locale, lang) => {
    //first call
    const wcStoreData = await store.dispatch(getWCStore());
    //now the rest can be async
    const [storeTextData, userData, seoData] = await Promise.all([
        await store.dispatch(getStoreText(locale, lang)),
        await store.dispatch(getUserData()),
        await store.dispatch(getSeoData())
    ]);
    return {
        wcStore: wcStoreData,
        userData: userData,
        storeText: storeTextData,
        seodata: seoData
    }
}
export default baseActions;