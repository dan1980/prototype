import actionTypes from './actionTypes';
import axios from 'axios';

export function getWCStore() {
    return dispatch => axios.get(process.env.ENV_API_ENDPOINT)
        .then(({ data }) => data)
        .then(wcstore => dispatch({
            type: actionTypes.GETWCSTORE,
            storeId: wcstore.storeId,
            catalogId: wcstore.catalogId,
            langId: wcstore.langId,
            domain: wcstore.domain
        }));
}