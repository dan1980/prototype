import { createStore, applyMiddleware } from "redux";
import thunkMiddleware from "redux-thunk";
import rootReducer from './reducers/index';

const globalState = {
    wcStore: null,
    userData: null,
    storeText: null,
    seoData: null
};

export function makeStore(initialState = globalState) {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunkMiddleware)
    );
}