import Router from 'next/router';

export default (context, target) => {
    if (context.res) {
        // server
        // 301: "Redirect"
        context.res.writeHead(301, { Location: target })
        context.res.end()
    } else {
        // In the browser, we just pretend like this never even happened ;)
        Router.replace(target)
    }
}